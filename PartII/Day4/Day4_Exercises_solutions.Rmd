---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 4 Exercises"
author: "Tina Davenport"
date: "November 1, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")


#############################################################################
##                                                                         ##
##  Author: tina.davenport@duke.edu                                        ##
##  Program: Day3_Exercises_solutions                                      ##
##  Date: 11/01/2019                                                       ##
##  Purpose: This program performs some of the tests outined in the notes  ##
##           and gives solutions to the in class exercises and examples    ##
##                                                                         ##
##                                                                         ##
##  Change Log:                                                            ##
##  11/01/2019 File created                                                ##
##                                                                         ##
#############################################################################

setwd("~/hiv-workshop-materials2019/PartII/Day4/")
library(tidyverse)
```


# In-Class Examples and Exercises

### Slide 8

```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")
  # tmp <- subset(ACTG, week==0);  summary(tmp)

# Research question: among white males in, is treatment A effective at
# improving HIV outcomes from baseline to week 4?

# Note: couldn't find "pivot_wider" function in tidyr or tidyverse, so
# converting back to base R code
ACTGa <- subset(ACTG, (week %in% c(0,4) & Arm=="A" & sex=="M" & race=="White"),
    select=c(ntisid, week, hivRNA, CD4, CD8))  # 32 white males in arm A

# reshape from long to wide - note: don't need to do this for t.test function!
ACTGa_wide <- reshape(data=ACTGa, v.names=c("hivRNA", "CD4", "CD8"),
    timevar="week", idvar="ntisid", direction="wide", sep="w")
ACTGa_wide$HIVd <- ACTGa_wide$hivRNAw4-ACTGa_wide$hivRNAw0
ACTGa_wide$CD4d <- ACTGa_wide$CD4w4-ACTGa_wide$CD4w0
ACTGa_wide$CD8d <- ACTGa_wide$CD8w4-ACTGa_wide$CD8w0
summary(ACTGa_wide)


# just look at CD4 counts for the first example
dat <- na.omit(subset(ACTGa_wide, select=c(ntisid, CD4w0, CD4w4, CD4d)))


#------------------  Step 1: Know data, check assumptions  -----------------#
par(mfcol=c(2,3))
hist(dat$CD4w0, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Pre-treatment CD4", freq=FALSE, main="Pre-treatment")
  lines(density(dat$CD4w0), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4w0), sd=sd(dat$CD4w0)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$CD4w0, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w0, col=2, lwd=3)

hist(dat$CD4w4, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Post-treatment CD4", freq=FALSE, main="Post-treatment")
  lines(density(dat$CD4w4), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4w4), sd=sd(dat$CD4w4)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$CD4w4, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w4, col=2, lwd=3)

hist(dat$CD4d, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Difference in CD4", freq=FALSE, main="Difference")
  lines(density(dat$CD4d), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4d), sd=sd(dat$CD4d)), add=TRUE, col=2, lwd=3)
qqnorm(dat$CD4d, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4d, col=2, lwd=3)


#-------------------  Step 4: Test statistic and p-value  ------------------#  
round(c(summary(dat$CD4d), SD=sd(dat$CD4d), N=length(dat$CD4d)), 2)

TT <- mean(dat$CD4d)/(sd(dat$CD4d)/sqrt(length(dat$CD4d)))
TT

# p-value:
pt(q=TT, df=length(dat$CD4d)-1, lower.tail=TRUE) - 
    pt(q=-TT, df=length(dat$CD4d)-1, lower.tail=TRUE)

# CI
tcrit <- qt(p=0.975, df=length(dat$CD4d)-1, lower.tail=TRUE)
mean(dat$CD4d)+c(-tcrit*sd(dat$CD4d)/sqrt(length(dat$CD4d)),
    tcrit*sd(dat$CD4d)/sqrt(length(dat$CD4d)))
```


### Slide 12-17

```{r}
# using t.test, can do it in several ways
t.test(x=dat$CD4w4, y=dat$CD4w0, alternative="two.sided", mu=0, paired=TRUE)
t.test(CD4~week, data=ACTGa[-which(ACTGa$ntisid==76872558),], alternative="two.sided", mu=0, paired=TRUE)
t.test(x=dat$CD4d, alternative="two.sided", mu=0)
```


### Slide 29

```{r}
Yis <- dat$CD4d[dat$CD4d !=0]  # WSR Step 2: remove all the zeros from Yi star
RR <- rank(abs(Yis), na.last=NA, ties.method="average") # WSR Step 3: ranks
SS <- sign(Yis)                                         # WSR Step 3: signs
cbind(Yis, RR, SS)                                      # Slide 29
tapply(RR, SS, sum)                                     # WSR Step 4: Test statistics


wilcox.test(dat$CD4d, alternative="two.sided", mu=0, paired=FALSE, exact=TRUE, correct=FALSE) 
wilcox.test(dat$CD4d, alternative="two.sided", mu=0, paired=FALSE, exact=TRUE, correct=TRUE) 
```


### Slide 37

```{r}
YY <- data.frame(TRT=c(1,1,1,2,2,2,2), Yi=c(-8, -1, -5, 2, -4, 6, 1))
YY$rank <- rank(YY$Yi)  # Rank the data
YY

getW <- function(x){    # A function to compute W
    n <- length(x)
    sum(x)-n*(n+1)/2
}
tapply(YY$rank, YY$TRT, getW)

# p-value 
wilcox.test(Yi~TRT, data=YY, alternative="less", mu=0, exact=TRUE, correct=FALSE)
# Same since there are no ties
# wilcox.test(Yi~TRT, data=YY, alternative="less", mu=0, exact=TRUE, correct=TRUE)

```


### Slide 41
```{r}
Oij <- matrix(c(43,19,7,11), nrow=2)                # observed counts
addmargins(Oij)
Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)  # Expected counts
  # 50*62/80; 50*18/80  
  # 30*62/80; 30*18/80  
addmargins(Eij)

XX <- sum((Oij-Eij)^2 / Eij)                        # Test Statistic
  # (43-38.75)^2/38.75 + (7-11.25)^2/11.25 +
  # (19-23.25)^2/23.25 + (11-6.75)^2/6.75

pchisq(q=XX, df=1, lower.tail=FALSE)  # p-value
chisq.test(x=Oij, correct=FALSE)      # using the function

#  or
HIV <- factor(c(rep("N", 50), rep("Y", 30)))
IVduse <- factor(c(rep("N", 43), rep("Y", 7), rep("N", 19), rep("Y", 11)))
# sample_n(data.frame(HIV, IVduse), 10)
chisq.test(x=HIV, y=IVduse, correct=FALSE)  
```


### Slide 60
```{r}
Oij <- matrix(c(230, 72, 192, 54, 63, 16, 15, 8), nrow=2)
Oij
# Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)
# sum((Oij-Eij)^2 / Eij)
chisq.test(x=Oij, correct=FALSE)
```


### Slide 63-64
```{r}
Oij <- matrix(c(99,80,70, 190,100,30, 96,20,10), nrow=3)
Oij
# Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)
# sum((Oij-Eij)^2 / Eij)
chisq.test(x=Oij, correct=FALSE)
```


### Slide 68-69
```{r}
Oij <- matrix(c(2, 5, 23, 30), nrow=2)
chisq.test(x=Oij, correct=FALSE)  # warning that some expected counts <5
outer(rowSums(Oij),colSums(Oij)) / sum(Oij)  # check Eij

fisher.test(Oij)  # use Fisher's test instead

```





<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1

Though the primary goal of the study was to assess the efficacy of three HIV medications on HIV outcomes (HIV RNA, CD4 counts, etc.) secondary outcomes included lipid measurements such as LDL and triglycerides. Among males in treatment A, did the study drug have an effect on change in triglycerides from baseline to week 24? Conduct a paired t-test.

a. First subset the ACTG data as needed
```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")  # read data
ACTGMale <- as_tibble(ACTG) %>%
    select(ntisid, week, Arm, sex, trigs)  %>%  # include only columns, participants,
    filter(sex == "M" & Arm == "A" & week %in% c(0, 24))  # and weeks of interest
# Base R
#ACTGMale <- subset(ACTG, (week %in% c(0,144) & Arm=="A" & sex=="M"),
#    select=c(ntisid, week, trigs))  # 32 white males in arm A


# Can reshape from long to wide...or not! Depending on how you use t.test
dat <- ACTGMale %>% spread(week, trigs) %>% rename(trigsw0="0", trigsw24="24")
dat$trigsd <- dat$trigsw24-dat$trigsw0
summary(dat)
```

b. Check assumptions
```{r}
par(mfcol=c(2,3))
hist(dat$trigsw0, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Pre-treatment Trigs", freq=FALSE, main="Pre-treatment")
  lines(density(dat$trigsw0), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$trigsw0), sd=sd(dat$trigsw0)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$trigsw0, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsw0, col=2, lwd=3)

hist(dat$trigsw24, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Post-treatment Trigs", freq=FALSE, main="Post-treatment")
  lines(density(na.omit(dat$trigsw24)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$trigsw24)), sd=sd(na.omit(dat$trigsw24))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$trigsw24, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsw24, col=2, lwd=3)

hist(dat$trigsd, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Difference in Trigs", freq=FALSE, main="Difference")
  lines(density(na.omit(dat$trigsd)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$trigsd)), sd=sd(na.omit(dat$trigsd))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$trigsd, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsd, col=2, lwd=3)
```

c. Perform the test
```{r}
t.test(x=dat$trigsw24, y=dat$trigsw0, alternative="two.sided", mu=0, paired=TRUE)
t.test(x=dat$trigsd, alternative="two.sided", mu=0)
```

d. State the conclusions

There is not enough evidence at the 0.05 level to conclude that the treatment is effective in lowering triglycerides from baseline to week 24.



### Question 2

Repeat Question 1 using a distribution free/nonparametric test.
```{r}
wilcox.test(x=dat$trigsw24, y=dat$trigsw0, alternative="two.sided", mu=0, paired=TRUE,
    exact=TRUE, correct=TRUE)
wilcox.test(x=dat$trigsd, alternative="two.sided", mu=0, exact=TRUE, correct=TRUE)
```



### Question 3
We found that treatment A was effective at increasing CD4 counts from baseline to week 4 among treatment naive white males with HIV. Is there a difference in treatment effect between treatment A and B on change in CD4 counts from baseline to week 4 in this sub population? 

a. First subset data as needed
```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")  # read data
ACTGMale <- as_tibble(ACTG) %>%
    select(ntisid, week, Arm, sex, race, CD4)  %>%  # include only columns and
    filter(sex=="M" & Arm %in% c("A", "B") & week %in% c(0, 4) &  # participants and
        race=="White") %>%                                        # weeks of interest
    droplevels()                      # remove factor levels that are 0 (e.g., trt C)
  
# reshape from long to wide
dat <- ACTGMale %>% spread(week, CD4) %>% rename(CD4w0="0", CD4w4="4")
dat$CD4d <- dat$CD4w4-dat$CD4w0
summary(dat)
```


b. Check assumptions
```{r}
#------------------  Step 1: Know data, check assumptions  -----------------#
par(mfcol=c(2,3))
hist(dat$CD4w0, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Pre-treatment CD4", freq=FALSE, main="Pre-treatment")
  lines(density(na.omit(dat$CD4w0)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$CD4w0)), sd=sd(na.omit(dat$CD4w0))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$CD4w0, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w0, col=2, lwd=3)

hist(dat$CD4w4, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Post-treatment CD4", freq=FALSE, main="Post-treatment")
  lines(density(na.omit(dat$CD4w4)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$CD4w4)), sd=sd(na.omit(dat$CD4w4))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$CD4w4, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w4, col=2, lwd=3)

hist(dat$CD4d, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Difference in CD4", freq=FALSE, main="Difference")
  lines(density(na.omit(dat$CD4d)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$CD4d)), sd=sd(na.omit(dat$CD4d))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$CD4d, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4d, col=2, lwd=3)

  
#Estimate CD4d densities and boxplots by trt group
#-------------------------------------------------------------------------#
mydens <- function(val, groups, ...){
    df <- na.omit(data.frame(val, groups))
    groups <- factor(df$groups)
    vals <- tapply(df$val, groups, identity)

    dens  <- lapply(vals, density)

    ylim <- range(unlist(lapply(dens, function(xl) xl$y)))
    xlim <- range(unlist(vals))

    plot(dens[[1]], lwd=3, xlim=c(xlim[1],xlim[2]), type="n",
        ylim=c(ylim[1],ylim[2]), cex.lab=1.2, cex.axis=1.2, cex.main=1.5,
        main="Density Plot", ...)
      for(ii in 1:length(dens)){
          lines(dens[[ii]], lwd=3, col=(2:7)[ii])
      }
      legend("topright", legend=levels(groups), col=2:7, lty=1, lwd=3,
          bty="n", cex=.8)
}
bxplot <- function(xax, yax, all=TRUE, ...){
    xax <- factor(xax)
    if(isTRUE(all)){ fin <- c(`Overall`=list(yax), split(yax, xax))
    } else { fin <- split(yax, xax) }

    boxplot(fin, lwd=2, cex.lab=1.5, cex.axis=1.5, pch=21,
        cex=1.25, cex.main=1.5, col=c("grey", palette()[2:7]), 
        bg=c("grey", palette()[2:7]), ...)
}
#-------------------------------------------------------------------------#

par(mfcol=c(1,3))
mydens(dat$CD4d, dat$Arm, xlab="Change in CD4 from Week 0 to 4")
bxplot(dat$Arm, dat$CD4d, xlab="Treatment Arm")

# both treatments seem effective since points are above y=x
plot(dat$CD4w0, dat$CD4w4, lwd=2, col=(2:3)[dat$Arm], cex.lab=1.5,
    cex.axis=1.5, cex.main=1.5, xlab="CD4 week 0", ylab="CD4 week 4")
abline(a=0, b=1, lwd=3, lty=3)


# summary statistics; are the variances equal for t-test?
mySummary <- function(x){ c(summary(x), SD=sd(x, na.rm=T), N=length(x))}
tapply(dat$CD4d, dat$Arm, mySummary)
var.test(CD4d~Arm, data=dat, ratio=1, alternative="two.sided")
```


b. Based on the plots of the data, is a t-test reasonable or would you prefer a nonparametric test? Either way, conduct both.
```{r}
# t-test is probably reasonable
t.test(CD4d~Arm, data=dat, alternative="two.sided", mu=0, paired=FALSE,
    var.equal=TRUE)

# nonparametric test
wilcox.test(CD4d~Arm, data=dat, alternative="two.sided", mu=0, paired=FALSE,
    exact=TRUE, correct=TRUE)
```



### Question 4

Is there a treatment difference between A and B on change in triglycerides from baseline to week 24 among males? Perform a parametric and nonparametric test.

a. First subset the ACTG data as needed
```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")  # read data
ACTGMale <- as_tibble(ACTG) %>%
    select(ntisid, week, Arm, sex, trigs)  %>%
    filter(sex=="M" & Arm %in% c("A", "B") & week %in% c(0, 24)) %>%
    droplevels()

# reshape from long to wide
dat <- ACTGMale %>% spread(week, trigs) %>% rename(trigsw0="0", trigsw24="24")
dat$trigsd <- dat$trigsw24-dat$trigsw0
summary(dat)
```


b. Check assumptions
```{r}
par(mfcol=c(2,3))
hist(dat$trigsw0, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Pre-treatment Trigs", freq=FALSE, main="Pre-treatment")
  lines(density(dat$trigsw0), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$trigsw0), sd=sd(dat$trigsw0)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$trigsw0, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsw0, col=2, lwd=3)

hist(dat$trigsw24, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Post-treatment Trigs", freq=FALSE, main="Post-treatment")
  lines(density(na.omit(dat$trigsw24)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$trigsw24)), sd=sd(na.omit(dat$trigsw24))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$trigsw24, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsw24, col=2, lwd=3)

hist(dat$trigsd, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Difference in Trigs", freq=FALSE, main="Difference")
  lines(density(na.omit(dat$trigsd)), col=4, lwd=3)
  curve(dnorm(x, mean=mean(na.omit(dat$trigsd)), sd=sd(na.omit(dat$trigsd))),
      add=TRUE, col=2, lwd=3)
qqnorm(dat$trigsd, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$trigsd, col=2, lwd=3)
  
  
# Estimate trigs densities and boxplots by trt group
#-------------------------------------------------------------------------#
mydens <- function(val, groups, ...){
    df <- na.omit(data.frame(val, groups))
    groups <- factor(df$groups)
    vals <- tapply(df$val, groups, identity)

    dens  <- lapply(vals, density)

    ylim <- range(unlist(lapply(dens, function(xl) xl$y)))
    xlim <- range(unlist(vals))

    plot(dens[[1]], lwd=3, xlim=c(xlim[1],xlim[2]), type="n",
        ylim=c(ylim[1],ylim[2]), cex.lab=1.2, cex.axis=1.2, cex.main=1.5,
        main="Density Plot", ...)
      for(ii in 1:length(dens)){
          lines(dens[[ii]], lwd=3, col=(2:7)[ii])
      }
      legend("topright", legend=levels(groups), col=2:7, lty=1, lwd=3,
          bty="n", cex=.8)
}
bxplot <- function(xax, yax, all=TRUE, ...){
    xax <- factor(xax)
    if(isTRUE(all)){ fin <- c(`Overall`=list(yax), split(yax, xax))
    } else { fin <- split(yax, xax) }

    boxplot(fin, lwd=2, cex.lab=1.5, cex.axis=1.5, pch=21,
        cex=1.25, cex.main=1.5, col=c("grey", palette()[2:7]), 
        bg=c("grey", palette()[2:7]), ...)
}
#-------------------------------------------------------------------------#

par(mfcol=c(1,3))
mydens(dat$trigsd, dat$Arm, xlab="Change in CD4 from Week 0 to 4")
bxplot(dat$Arm, dat$trigsd, xlab="Treatment Arm")
plot(dat$trigsw0, dat$trigsw24, lwd=2, col=(2:3)[dat$Arm], cex.lab=1.5,
    cex.axis=1.5, cex.main=1.5, xlab="Trigs week 0", ylab="Trigs week 24")
abline(a=0, b=1, lwd=3, lty=3)


# summary statistics; are the variances equal for t-test?
mySummary <- function(x){ c(summary(x), SD=sd(x, na.rm=T), N=length(x))}
tapply(dat$trigsd, dat$Arm, mySummary)
var.test(trigsd~Arm, data=dat, ratio=1, alternative="two.sided")
```


c. Perform tests
```{r}
# one variance is double the other and VRT was significant
t.test(trigsd~Arm, data=dat, alternative="two.sided", mu=0, paired=FALSE,
    var.equal=FALSE)

# nonparametric test
wilcox.test(trigsd~Arm, data=dat, alternative="two.sided", mu=0, paired=FALSE,
    exact=TRUE, correct=TRUE)
```


d. Draw conclusions

The mean change in triglycerides from baseline to 24 weeks was not different between the two treatment groups but the median change was. Because of the shapes of the distributions in the density plots and the relatively small samples sizes within the two treatment groups, it's hard to say if comparing the means is appropriate.



### Question 5
For the ACTG data, sampling was from one target population, thus, if we were to take a different sample, we would expect the counts for different characteristics (e.g., how many men and women there are in the trial) to change.

a. Thought question: Would chi-squared tests of characteristics of participants be one of homogeneity or independence?

b. At baseline, is there an association between race (Blacks and Whites only) and being sure that HIV medications have a positive effect (`howsure2`)?
```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")


# subset to the data I need and clean up
ACTGbl <- ACTG %>% filter(week==0 & race !="Other") %>%
    select(ntisid, race, howsure2) %>% droplevels()   # include only columns needed
summary(ACTGbl)
addmargins(table(ACTGbl$race, ACTGbl$howsure2, exclude=NULL))


# do chi-squared test first
chisq.test(x=ACTGbl$race, y=ACTGbl$howsure2, correct=FALSE)

# small expected counts, so do Fisher as sensitivity
fisher.test(x=ACTGbl$race, y=ACTGbl$howsure2)
```


b. What about being "extremely sure" vs. all others and race? 
```{r}
# dichotomize "howsure"
ACTGbl$howsureDich <- ifelse(ACTGbl$howsure2=="Extremely sure", 1, 0)
  # table(ACTGbl$howsure2, ACTGbl$howsureDich, exclude=NULL)  #check!


# do chi-squared test first
chisq.test(x=ACTGbl$race, y=ACTGbl$howsureDich, correct=FALSE)
```


c. Among men, is there an association between partner preference and race at baseline? 
```{r}
# subset to the data I need and clean up
ACTGbl <- ACTG %>% filter(week==0 & race !="Other" & sex=="M") %>%
    select(ntisid, race, sexpref) %>% droplevels()   # include only columns needed
summary(ACTGbl)
addmargins(table(ACTGbl$race, ACTGbl$sexpref, exclude=NULL))


# do chi-squared test first
chisq.test(x=ACTGbl$race, y=ACTGbl$sexpref, correct=FALSE)

# small expected counts, so do Fisher as sensitivity
fisher.test(x=ACTGbl$race, y=ACTGbl$sexpref)
```


d. Which observed count(s) tend to deviate most from the expected counts?
```{r}
Oij <- table(ACTGbl$race, ACTGbl$sexpref)
Oij
Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)
Eij
(Oij-Eij)^2 / Eij

# the first row, second column contributies the most to the chi-squared statistic.
# This means more black men prefer women only than what we would expect if the
# null were true.
```




### Question 6

Researchers investigated the relationship between HIV seropositivity and STIs in a population at high risk for sexual acquisition of HIV. Subjects were 144 female commercial sex workers in Thailand of whom 62 were HIV-positive and 109 had an STI. In the HIV-negative group, 51 had a history of STD.

a. Estimate the risk difference, relative risk, odds, and odds ratio (and CIs) of HIV seropositivity.
```{r}
# can hardcode all the formulas or write a function. The format for the
# formulas must be that n11 is risk factor present/outcome present, n12 is
# risk factor present/outcome absent, n21 is risk factor absent/outcome
# present, and n22 is risk factor absent/outcome absent.
n11 <- 58
n12 <- 51
n21 <- 4
n22 <- 31
pi1 <- n11/(n11+n12)
pi2 <- n21/(n21+n22)

#----------------------------  Risk Difference  ----------------------------#
pi1-pi2              # risk difference
ll <- pi1-pi2-1.96*  # Lower CI
    sqrt(((pi1*(1-pi1))/(n11+n12)+(pi2*(1-pi2))/(n21+n22)))
lu <- pi1-pi2+1.96*  # Upper CI
    sqrt(((pi1*(1-pi1))/(n11+n12)+(pi2*(1-pi2))/(n21+n22)))
c(ll,lu)             # CI


#-----------------------------  Relative Risk  -----------------------------#
pi1/pi2         # relative risk
ll <- log(pi1/pi2)-1.96*sqrt((n12/(n11*(n11+n12))+n22/(n21*(n21+n22))))
lu <- log(pi1/pi2)+1.96*sqrt((n12/(n11*(n11+n12))+n22/(n21*(n21+n22))))
exp(c(ll, lu))  # CI


#--------------------------  Odds and Odds Ratio  --------------------------#
Odds_p <- pi1/(1-pi1)  # odds among positive STI
Odds_n <- pi2/(1-pi2)  # odds among negative STI

Odds_p/Odds_n          # Odds ratio
ll <- log(Odds_p/Odds_n)-1.96*sqrt((1/n11+1/n12+1/n21+1/n22))
lu <- log(Odds_p/Odds_n)+1.96*sqrt((1/n11+1/n12+1/n21+1/n22))
exp(c(ll, lu))         # CI




# Write a function. The format for the formulas must be that n11 is risk
# factor present/outcome present, n12 is risk factor present/outcome absent,
# n21 is risk factor absent/outcome present, and n22 is risk factor
# absent/outcome absent.
#---------------------------------------------------------------------------#
getMs <- function(tbl, aa=0.05){  # compute RR, OR, and CIs from 2x2 table
    za2 <- qnorm(aa/2)  # N(0,1) critical value
    n11 <- tbl[1,1];  n12 <- tbl[1,2];  
    n21 <- tbl[2,1];  n22 <- tbl[2,2];  
    p1 <- n11/(n11+n12);  p2 <- n21/(n21+n22)

    #-----------------------  Risk Difference and CI  ----------------------#
    rd <- p1-p2
    rdSE <- sqrt( p1*(1-p1)/(n11+n12) + p2*(1-p2)/(n21+n22) )
    rdCI <- rd +c(za2*rdSE, -za2*rdSE)

    #-------------------------  Risk Ratio and CI  -------------------------#
    rr <- p1/p2 
    rrSE <- sqrt( n12/(n11*(n11+n12)) + n22/(n21*(n21+n22)) )
    rrCI <- exp( log(rr) +c(za2*rrSE, -za2*rrSE) )

    #-------------------------  Odds Ratio and CI  -------------------------#
    or <- (p1/(1-p1)) / (p2/(1-p2))
    orSE <- sqrt( 1/n11 +1/n12+1/n21+1/n22 )
    orCI <- exp( log(or) +c(za2*orSE, -za2*orSE) )

    ans <- data.frame(c(rd, rr, or), c(rdCI[1], rrCI[1], orCI[1]),
        c(rdCI[2], rrCI[2], orCI[2]))
    rownames(ans) <- c("RD", "RR", "OR")
    colnames(ans) <- c("Estimate", paste0("Lower ", 100*aa/2, "%"),
        paste0("Upper ", 100*(1-aa/2), "%"))
    return(ans)
}
getMs(matrix(c(58, 4, 51, 31), nrow=2))
```


b. what are the conclusions from each measure?

RD: Those with positive STI have 0.42 increased risk of HIV compared to those without STI.

RR: Those with positive STI have over 4 times the risk of HIV compared to those without STI.

OR: PThose with positive STI have over 8 times the odds of of HIV compared to those without STI.

The CIs indicate these increased risks and odds are statistically significant at the 0.05 level.




