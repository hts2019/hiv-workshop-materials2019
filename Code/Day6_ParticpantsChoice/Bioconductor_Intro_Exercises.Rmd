---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Introduction to Bioconductor"
author: "Janice M. McCarthy"
date: "September 27, 2019"
output: html_document
---

```{r, setup, echo = FALSE, message=FALSE, warning=FALSE, root.dir = "~/hiv-workshop-materials2019/Code/Day4_IntroductionToR/"}
library(tidyverse)
library(stringr)
library(knitr)
```

# BioConductor

We've already seen standard R packages that are house in CRAN (Comprehensive R Archive Network). 
CRAN is home to many, many R packages. But there is a whole other world out there when it comes to bioinformatics in R. It's called [BioConductor](https://bioconductor.org/). BioConductor is a comprehensive toolkit for all things having to do with high-throughput sequencing data processing and analysis. In the 'Assays' portion of this workshop, we will be using several bioconductor packages, in particular, packages for analysis of microbiome data and flow cytometry data.

## Installing BioConductor packages

BioConductor has it's own installation procedure (and it's own criteria for documentation, testing, etc.) - separate from CRAN. Let's have a look at the page for [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)

## S4 Objects

R has two main types of 'objects' that can work with 'methods' (functions that are specific to those objects): S3 and S4. S3 objects are really just lists with the `class` attribute set. For example:


```{r}
cat <- list(name = "Dash", age = 10)
class(cat) <- "Pet"

dog <- list(name = "Tibbs", age = 8)
class(dog) <- "Pet"

print.Pet <- function(x){
    print(paste("Name:", x$name, "Age:", x$age))
}

print(cat)
```

S4 objects are a bit more stringent. We won't contruct our own right now, just mention that they are created with constructor functions that make sure there is consistency.

Bioconductor makes extensive use of S4 objects. One of the main features of these is inheritance. This means that if an object is a 'child' of an existing object, the child inherits all the features and *functions* (methods) of the parent.

Understanding the structure of these objects is key to understanding BioConductor packages, because they are all set up to work with data in a particular format. Think of this as an extension of how ggplot works together with the structure of a tibble to create complex plots with a few instructions.

Let's look at some of the simpler Bioconductor objects that will be used to build up more complicated structures.

### Biostrings
```{r}
suppressPackageStartupMessages({
  library(Biostrings)
})
dna <- DNAStringSet( c("AAACTG", "CCCAACCA") ) #DNAStringSet is a constructor function for the class DNAStringSet

dna
reverseComplement(dna)  #This is a method that works on a DNAStringSet object

```

Note that the method `reverseComplement` *only* works on a DNAStringSet (or an object that is a child of DNAStringSet).

```{r}
# This doesn't work

dna2 <- c("AAACTG", "CCCAACCA")
#reverseComplement(dna2)
```

Where to find help:
  
- `class(dna)`
- `?"DNAStringSet"`, `?"reverseComplement,DNAStringSet-method"` (tab
                                                                 completion!)
- `browseVignettes(package="Biostrings")`
- https://bioconductor.org/packages
- https://support.bioconductor.org

[Biostrings]: https://bioconductor.org/packages/Biostrings

```{r}
?DNAStringSet
```

### GenomicRanges


```{r}
suppressPackageStartupMessages({
  library(GenomicRanges)
})

# GRanges is a contructor function

gr <- GRanges(
  c("chr1:10-19", "chr1:15-24", "chr1:30-39")
)
gr
```

- Closed-interval (start and end coordinates include in range, like
                   Ensembl; UCSC uses 1/2-open)
- 1-based (like Ensembl; UCSC uses 0-based)

Operations

If we ask for help for GRanges, we can see the list of methods that can be applied to such an object.

Here, we can also see the values that can be set within the GRanges object.

```{r}
(help("GRanges"))
width(gr)
```


```{r}
shift(gr, 1)
shift(gr, c(1, 2, 3))
```


### GRangesList

- List of `GRanges`, e.g., exons

We can construct a GRangesList from a GRanges object as follows:


```{r}
# Assign a gene name to each element of the GRanges object
gene <- c("A", "A", "B")

# Use the splitAsList function from GRanges (actually IRanges)
grl <- splitAsList(gr, gene)
grl
```
In this example, gene A has two exons on chromosome 1, at positions 10-19 and 15-24. Gene B has one exon on chromosome 1, starting at position 30 and ending at 39. (Yes, that's a bit small for a gene!)

Many of the high-throughput analysis pipelines work with an object called a 'SummarizedExperiment' or 'RangedSummarizedExperiment`.  We'll work with a sample data set from BioConductor.
## Airway example


```{r}
library("airway")
data("airway")
se <- airway
```


```{r}
se
```

[This tutorial](https://bioconductor.org/packages/devel/bioc/vignettes/SummarizedExperiment/inst/doc/SummarizedExperiment.html) gives a great introduction to the SummarizedExperiment object. We'll take a peek, and then move on to using DESeq2 to analyze the data.

```{r}
assays(se)
```


```{r}
assays(se)$counts %>% head()
```


```{r}
rowRanges(se)
```
`rowData` is specific to SummarizedExperiment
```{r}
rowData(se)
```


```{r}
colData(se)
```

We can use tidyverse on these objects, but not without conversion. Also, we have 'masked' the `filter` function from `dplyr`.
```{r}
colData(se) %>% dplyr::filter(cell == "N61311")

colData(se) %>% as_tibble() %>% dplyr::filter(cell == "N61311")
```

```{r}
metadata(se)
```


```{r}
# Just a list - we can add elements

metadata(se)$formula <- counts ~ dex + albut

metadata(se)
```
When using these large BioConductor objects, subsetting is best done with the 'base R' syntax. This is because it isn't really 'base R'. The BioConductor object has its own subsetting function. Consider the following:

```{r}
# subset the first five transcripts and first three samples
subset_se <- se[1:5, 1:3]
subset_se
```
Note, this is still a RangedSummarizedExperiment. The subsetting command has automatically operated on all relevant parts of the object.

```{r}
assays(subset_se)$counts

```
```{r}
rowRanges(subset_se)
```


```{r}
colData(subset_se)
```
This behavior is an important feature - if we drop a sample or a transcript, it will be removed from the entire object. This type of error is common, so it is built-in to BioConductor to prevent users from making it.

Bioinformatics pipelines typically have the following outline:

- Read in raw data (fastq files, idat, etc).
- Perform QC on raw data
- Possibly align to genome

The above steps may be done at the unix command line, as they are computationally intensive. The following is done in Bioconductor:

- Read pre-processed and/or aligned data
- Further QC, possibly with visualization
- Normalization procedures
- Analysis 

The airway data has been pre-processed and is ready with gene counts. We can use the DESeq2 package to perform differential expression analysis.

First, we will create another S4 object from our RangedSummarizedExperiment object.

```{r}
library("DESeq2")

dds <- DESeqDataSet(se, design = ~ cell + dex)
dds


```

Lets's look at the help page for DESeqDataSet:

```{r}
help(DESeqDataSet)
```
Where did DESeq put the design argument? 

In S3 (regular R) objects or lists, we access elements with a `$`. S4 objects have 'slots' that are accessed using the `@` sign. 

```{r}
dds@design
```
To see the other slots, we can type `dds@` and press the `tab` key.

Design is our model. This tells R we want to model differential expression as a function of cell type and steroid treatment.

We'll do a few common manipulation steps:
```{r}
# remove rows with less than 10 total transcripts

keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]

dds$dex

```
Note that the `reference level` is by default set alphabetically, so the '0' level is 'trt'. That means we'll obtain a regression coefficient for 'untrt' as compared to 'trt'. We probably want to have a regression coefficient for 'trt' as compared to 'untrt'.

```{r}
# Specify reference level

dds$dex <- factor(dds$dex, levels = c("untrt","trt"))

#alternative
dds$dex <- relevel(dds$dex, ref = "untrt")

dds$dex
```


```{r}
ddsDE <- DESeq(dds)
res <- results(ddsDE)
res
```
What kind of object is ddsDE?

```{r}
ddsDE
```

What about res?

```{r}
class(res)
```

Exercises:

1. Select the top 10 hits in the DESeq analysis by adjusted p-value. 
2. Use the DESeq2 function rlog to create normalized count data, then plot the principle components using plotPCA. (We can talk about what this means). Do this for both cell type and dex.
3. Use plotCounts to plot the normalized gene counts by dex group for the gene with the lowest pvalue.

## Methylation Example

The following code loads a test data set for the package `ChAMP`. `ChAMP` is really a collection of Bioconductor packages put together for convenience.
```{r}
suppressPackageStartupMessages(library(ChAMP))
suppressPackageStartupMessages(library(ChAMPdata))
data("testDataSet")
```

Exercises

1. Investigate the object `myLoad`. It was created when we loaded `testDatSet` above. What kind of objects does it contain? (There are some values of the list `myLoad` that are empty. That is because `mset` and `rgset` are raw values and this data set is in a sense 'partially processed'. We won't worry about these, as we are just trying to understand the fundamental concepts of these objects and the functions that work on them.)

2. Normalize the beta values using the function `champ.norm'. Inspect the resulting object.

3. From the normalized values, find differentially methylated probes using champ.DMP

4. Use DMRcate to find differentially methylated regions. Visualize the top DMR.

5. Perform a gene set enrichment analysis.
